import React from "react";
import { StyleSheet, View, SafeAreaView, ScrollView } from "react-native";
import { Header } from "./src/components";

import { MainScreen } from "./src/screens";

export default function App() {
  return (
    <View style={styles.container}>
      <SafeAreaView>
        <ScrollView>
          <Header></Header>
          <MainScreen></MainScreen>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f7f8fa'
  }
});
