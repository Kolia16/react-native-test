import React from "react";
import { StyleSheet, View, Text, ImageBackground, Alert } from "react-native";
import { Icon } from "react-native-elements";
import { LinearGradient } from "expo-linear-gradient";

import { IconCard } from "../components";

import { h } from "../constants";
import { THEME } from "../theme";

export const MainScreen = () => {
  const arr = [
    { icon: "thumb-tack", text: "Atrakcje" },
    { icon: "map", text: "Szlaki" },
    { icon: "briefcase", text: "Baza turystyczna" },
    { icon: "bars", text: "Planner" },
    { icon: "cutlery", text: "Produkty regionaine" },
    { icon: "map-marker", text: "Questy" },
    { icon: "umbrella", text: "Pogoda" },
    { icon: "qrcode", text: "Skaner QR" },
    { icon: "calendar", text: "Kalendarz wydarzen" },
  ];
  return (
    <View style={styles.container}>
      <Text style={styles.h1}>Aktualosci</Text>

      <ImageBackground
        style={styles.image}
        imageStyle={{ borderRadius: 30 }}
        source={require("../../assets/180006.429103.1996.jpg")}
      >
        <LinearGradient
          style={styles.linearGradient}
          colors={[THEME.COLOR_1, "rgba(0,0,0,0)"]}
        ></LinearGradient>

        <Text style={styles.h3}>Doroczny Festyn juz w najlizsza ssobote</Text>

        <View style={styles.icon}>
          <Icon
            raised
            name="plus"
            type="font-awesome"
            size={18}
            color={THEME.COLOR_1}
            onPress={() => {
              Alert.alert(
                "Add",
                "My Alert Msg",
                [
                  {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel",
                  },
                  { text: "OK", onPress: () => console.log("OK Pressed") },
                ],
                { cancelable: false }
              );
            }}
          />
        </View>
      </ImageBackground>

      <View style={styles.main}>
        {arr.map((obj, idx) => {
          return <IconCard key={idx} obj={obj}></IconCard>;
        })}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 15,
  },
  h1: {
    fontSize: 22,
    fontWeight: "bold",
    paddingStart: 20,
    marginBottom: 10,
  },
  image: {
    width: "100%",
    position: "relative",
    height: h / 3.5,
    marginBottom: 20,
  },
  linearGradient: {
    marginBottom: 110,
    flex: 1,
    borderRadius: 30,
    height: 100,
  },
  h3: {
    fontSize: 16,
    color: "#fff",
    position: "absolute",
    width: 160,
    left: 20,
    top: 20,
  },
  icon: {
    position: "absolute",
    top: 10,
    right: 10,
  },
  main: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "space-between",
  },
});
