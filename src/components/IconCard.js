import React from "react";
import { StyleSheet, View, Text, Alert } from "react-native";
import { Icon } from "react-native-elements";

import { w } from "../constants";
import { THEME } from "../theme";

export const IconCard = ({ obj }) => {
  return (
    <View style={styles.container} >
      <View style={styles.boxIcon}>
        <Icon
        onPress={
            ()=>{
                Alert.alert(
                    `${obj.text}`,
                    "My Alert Msg",
                    [
                      {
                        text: "Cancel",
                        onPress: () => console.log("Cancel Pressed"),
                        style: "cancel",
                      },
                      { text: "OK", onPress: () => console.log("OK Pressed") },
                    ],
                    { cancelable: false }
                  );
            }
        }
          containerStyle={styles.icon}
          name={obj.icon}
          size={60}
          type="font-awesome"
          color={THEME.COLOR_1}
        />
      </View>
      <Text style={styles.text}>{obj.text}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    width: (w - 42) / 3,
    marginHorizontal: 2,
    marginVertical: 10,
  },
  boxIcon: {
    width: "100%",
    marginBottom: 10,
    flex: 1,
    alignItems: "center",
    padding: 0,
  },
  icon: {
    width: 100,
    padding: 20,
    borderRadius: 25,
    backgroundColor: "#fff",

    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
  },
  text: {
    textAlign: "center",
    fontSize: 13,
  },
});
